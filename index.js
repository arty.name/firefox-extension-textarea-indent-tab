document.addEventListener('keydown', async (event) => {
  const { target, shiftKey, altKey, metaKey, ctrlKey, key} = event;

  // ignore all keypresses which are not Tabs, or have Ctrl or Alt or Meta pressed
  if (key !== 'Tab' || ctrlKey || altKey || metaKey) return;

  // ignore all keypresses not inside textareas
  if (!/textarea/i.test(target.nodeName)) return;

  const start = target.selectionStart;
  const end = target.selectionEnd;
  // ignore events when no text is selected
  if (start === end) return;

  const { value } = target;
  const before = value.substring(0, start);
  const selection = value.substring(start, end);
  const after = value.substring(end);

  // ignore text selections that do not span several lines
  if (!/[\n\r]/.test(selection)) return;

  // prevent moving focus to the next control
  event.preventDefault();

  const { spaces = 4 } = (await browser.storage.local.get('spaces'))
  const indent = ' '.repeat(spaces);

  const updated = !shiftKey ?
    // when shift is not pressed insert spaces after every newline
    indent + selection.replace(/([\r\n])(?!$)/gm, `$1${indent}`) :
    // when shift is pressed remove spaces after every newline
    selection.replace(new RegExp(`(^|[\r\n]+)${indent}`, 'g'), '$1');

  // construct the new value for textarea
  target.value = before + updated + after;
  // and restore the selection
  target.selectionStart = start;
  target.selectionEnd = start + updated.length;
}, false);
